"use strict";

//Maps in d3
var canvas = d3.select("body").append("svg").attr("width", 760).attr("height", 700);
d3.json("LA-22-louisiana-parishes.json", function (data) {
  var group = canvas.selectAll("g").data(data.features).enter().append("g"); //projection for maps is new

  var projection = d3.geo.mercator().scale(7300).attr('transform', 'translate(' + 0 + ',' + 1980 + ')');
  var path = d3.geo.path().projection(projection);
  var areas = group.append("path").attr("d", path).attr("class", "area").attr("fill", "steelblue");
  group.append("text").attr("x", function (d) {
    return path.controid(d)[0];
  }).attr("y", function (d) {
    return path.controid(d)[1];
  }).attr("text-anchor", "middle").text(function (d) {
    return d.properties.LNNAMN;
  });
});